# Frontend Mentor - Interactive rating card component solution

This is a solution to the [Interactive rating card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/interactive-rating-component-koxpeBUmI). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size
- See hover and focus states for interactive elements

This is a basic project, but it was done to:
* practicing fundamental concepts
* create my fist Vite project
* Deploy to Gitlab pages

### Links

- Live Site URL: [Live Solution](https://frontendchallenges.gitlab.io/interactive-pricing/)

## My process

1. Create project `yarn vite create`
   Add dependencies `yarn add --dev sass`
2. Create tokens.scss with the constant values for colors, fonts, and sizes, and styles.scss for general styling
  The styles.scss must be included in main.js and tokens.scss in vite.config.ts
3. Set a backgroud color for body
4. Think of the design by splitting the layout in Components:
  App    
    Pricing (main)
      *Background
      Header
        Title
        Summary
      Card
        Body
          span.stats
          slider
          span.price
          toggle (checkbox)
        footer
          features
          cta
    Attribution
5. Create a card center vertically and horizontally
6. Think about the desktop design. 
* Change the order of the stats, pric and slider. Also the discount badge must change from
* "-25%" to "25% discount".
* The features and cta must be in the same row.
7. Notice and implement the active states.
8. This challenge also requires to change the view when selecting the Submit button. Some people would modify the DOM structure, but I think it's cleaner to use router to change to the ThankComponent.
9. Deploy to gitlab pages (remember configure the base, e.g. vite build --base=./)

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- Vue + Vite
- VueRouter

### Lessons

We can change the order of flexbox children, by the attribute order: 1;

```css
&--stats {
      order: 1;
    }
    &--price {
      order: 2;
    }
    &--slider {
      order: 3;
    }    
```

To create toggle button, the main idea is to hide the input, and handle, before and after elements to show a circle and move it to the right when it is checked.
```css
 .toggle:before {
  content: "";
  width: 1.3em;
  height: 1.3em;
  display: block;
  position: absolute;
  left: 4px;
  top: 2px;
  border-radius: 50%;
  background: $color-pricing-bg;
 }
 .toggle:checked:before {
  left: 24px;
 }
```
 To create the slider, also, hide the input, draw a colored circle, and a div (progress) that changes its background from 0 to $sliderValue
```css
// fill area
    &::before {
      content: "";
      position: absolute;
      left: 0;
      right: 0;
      clip-path: inset(0% 100% 0 calc(var(--sliderValue)*1%));
    }
```
html```
<div class="range-slider">
  <input
    type="range"
    min="0"
    max="100"
    step="1"
    v-model="sliderValue"      
  />
  <div class="range-slider__progress" v-bind:style="{ '--sliderValue': sliderValue}"></div>
</div>
```
```js
let sliderValue= ref("55");
```

### Continued development

This is one of many projects, I plan to build to make more experience with FrontEnd development, specially with the Framework, handling spacing and practicing css.

**Note: Delete this note and the content within this section and replace with your own plans for continued development.**

### Useful resources

- [How to deploy a Static Site](https://vitejs.dev/guide/static-deploy.html)
- [Router](https://next.router.vuejs.org/guide/essentials/passing-props.html#named-views)
- [To create a toggle button](https://codeconvey.com/convert-checkbox-to-toggle-button-css/)
- [To create a slider](https://codepen.io/vsync/pen/mdEJMLv)

## Author

- Website - [Víctor HG](https://gitlab.com/vianhg)
- Frontend Mentor - [@vianhg](https://www.frontendmentor.io/profile/vianhg)

## Acknowledgments

Always to God and my family.